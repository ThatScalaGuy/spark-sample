from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
from pyspark.sql.window import Window

spark = SparkSession.builder.getOrCreate()

data = spark.read.parquet("data/veh_traj_df.parquet")

# data.show(10, False)


@F.udf(returnType=T.ArrayType(T.ArrayType(T.ArrayType(T.IntegerType()))))
def magic(values):
    return values


w = Window.orderBy("time").rowsBetween(0, 10)

data = data.withColumn("pls_name_it", magic(F.collect_list("Veh_touch_grids_range").over(w)))

data.show(100, False)

