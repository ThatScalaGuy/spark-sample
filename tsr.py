from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
from pyspark.sql.window import Window

spark = SparkSession.builder.appName("TSR").getOrCreate()

dut = spark.read.option("header", True).option("inferSchema", True).option("sep", ";").csv('data/tsr/dut.csv')
reference = spark.read.option("header", True).option("inferSchema", True).option("sep", ";").csv('data/tsr/reference.csv')

dut.printSchema()
reference.printSchema()

window = Window.partitionBy("Carmen-Time", "Carmen-Xposition").orderBy("_diff")

data = dut.crossJoin(reference)
data\
    .withColumn("_diff", F.abs(data["Carmen-Time"] - data["DGPS-time"]))\
    .withColumn("_rank", F.row_number().over(window))\
    .filter(F.col("_rank") == 1)\
    .orderBy(F.col("Carmen-Time"))\
    .show()