from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.window import Window

spark = SparkSession.builder.appName("Lidar").getOrCreate()
data = spark.read.option("header", "true").option("sep",";").option("inferSchema", "true").format("csv").load("data/OpenRoad5Seconds.CSV")

data.show()


def get_start_end_timestamps(df):
    window = Window.orderBy("timestamp")

    df = df.select("timestamp", "unobstructed") #temp für debuggen
    df = df.withColumn("_delta", F.lag(df["unobstructed"]).over(window))
    df = df.withColumn("_flip", (df["unobstructed"] != df["_delta"]) | (df["_delta"].isNull()))
    df = df.withColumn("_start", F.when(df["_flip"], df["timestamp"]))
    df = df.withColumn("start", F.last(df["_start"], ignorenulls=True).over(window))
    return df.groupBy(df["start"]).agg(F.max(df["timestamp"]).alias("end"), F.first(df["unobstructed"]).alias("unobstructed"))


get_start_end_timestamps(data)\
    .filter(F.col("end") - F.col("start") >= 5)\
    .filter(F.col("unobstructed") == 1)\
    .show()
