from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T

spark = SparkSession.builder.appName("Lidar").getOrCreate()

matching = spark.read.parquet("data/lidar/Matching.parquet")

matching.printSchema()

dt = spark.read.parquet("data/lidar/DT.parquet")

dt.printSchema()

gt = spark.read.parquet("data/lidar/Gt.parquet")
gt.printSchema()
gt = gt.select("META:GTID", "DATA:Attributes_root_Movement",
               "DATA:Content_ItemClass", "DATA:Attributes_BB_Distance",
               "DATA:Attributes_BB_Direction_x") \
    .withColumn("distance", F.col("DATA:Attributes_BB_Distance") * F.col("DATA:Attributes_BB_Direction_x")) \
    .filter(F.col("DATA:Attributes_root_Movement") == F.lit(0)) \
    .filter(F.col("DATA:Content_ItemClass") == F.lit(1)) \
    .filter(F.col("distance") < 300)



bin_gt = gt.withColumn("bin", F.floor(F.col("distance") / F.lit(5)) * F.lit(5))
bin_gt.groupBy(F.col("bin")).agg(F.count(F.col("*"))).orderBy(F.col("bin").asc()).show()


# [4,9), [9,14), ......


def lidar_range(gt, dut, matching, movement=0, item_class=1, start_distance=4, end_distance=300, bucket_size=5):
    bin_range = spark.range(start_distance, end_distance, bucket_size).withColumnRenamed("id", "bin")

    true_positive_matching = matching.select("DATA:Matching_GTID", "DATA:Matching_DUTID", "DATA:Matching_TimeStamp") \
        .filter(F.col("DATA:Matching_DUTID") != F.lit(-1)) \
        .filter(F.col("DATA:Matching_GTID") != F.lit("-1"))

    gt = gt.select("META:GTID", "DATA:Attributes_root_Movement",
                   "DATA:Content_ItemClass", "DATA:Attributes_BB_Distance",
                   "DATA:Attributes_BB_Direction_x") \
        .filter(F.col("DATA:Attributes_root_Movement") == F.lit(movement)) \
        .filter(F.col("DATA:Content_ItemClass") == F.lit(item_class))

    gt = true_positive_matching.join(gt, true_positive_matching["DATA:Matching_GTID"] == gt["META:GTID"]) \
        .withColumn("distance", gt["DATA:Attributes_BB_Distance"] * gt["DATA:Attributes_BB_Direction_x"]) \
        .withColumn("bin", F.floor((F.col("distance") - F.lit(start_distance)) / F.lit(bucket_size)) * F.lit(
        bucket_size) + F.lit(start_distance)) \
        .filter(F.col("distance") < F.lit(end_distance)) \
        .groupBy(F.col("bin")).agg(F.count(F.col("*"))).orderBy(F.col("bin").asc()).show()

    dut = dut.select("META:DUTID", "META:Timestamp", "DATA:Attributes_BB_Distance", "DATA:Attributes_BB_Direction_x")

    dut = true_positive_matching.join(dut, (true_positive_matching["DATA:Matching_DUTID"] == dut["META:DUTID"]) & (
            true_positive_matching["DATA:Matching_TimeStamp"] == dut["META:Timestamp"])) \
        .withColumn("distance", dut["DATA:Attributes_BB_Distance"] * dut["DATA:Attributes_BB_Direction_x"]) \
        .withColumn("bin", F.floor((F.col("distance") - F.lit(start_distance)) / F.lit(bucket_size)) * F.lit(
        bucket_size) + F.lit(start_distance)) \
        .groupBy(F.col("bin")).agg(F.count(F.col("*"))).orderBy(F.col("bin").asc()).show()

    return None


lidar_range(gt=gt, dut=dt, matching=matching)
