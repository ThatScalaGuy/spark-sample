from pyspark import SparkContext, SparkConf

conf = SparkConf().setAppName("RDDTest").setMaster("local[*]")
sc = SparkContext(conf=conf)



data = sc.textFile("data/sample.txt")
data = sc.range(0,100,5)
data = sc.parallelize([(1,2,3), (3,4,5)])
#
# line_len = data.map(lambda line: (line, len(line)))
# line_len.foreach(print)
#
single_words = data.flatMap(lambda line: line.split())

#
# data.foreach(print)

def word_count( data ):
    return (data, 1)


single_words.map(word_count).reduceByKey(lambda a, b: a + b).foreach(print)
