from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T

spark = SparkSession.builder.appName("DataFrameTest").getOrCreate()
carrier = spark.read.option("header", True).csv("data/L_UNIQUE_CARRIERS.csv")

data = spark.read.parquet("data/ONTIME_REPORTING")
data.printSchema()

# Simple queries
# 1.) List of all a) carriers sorted / b) count
# carriers = data.select(data["OP_UNIQUE_CARRIER"]).distinct()
# carriers.sort(data["OP_UNIQUE_CARRIER"].asc()).show()
#
# print(carriers.count())

# 2.) List of all origin / count
#data.select("ORIGIN").distinct().sort(data["ORIGIN"].desc()).show()
#print(data.select("ORIGIN").distinct().count())
#print(data.select(data["ORIGIN"]).union(data.select(data["DEST"])).distinct().count())

# 3.) distance ORIGIN to DEST order by distance

# data.select("ORIGIN", "DEST", "DISTANCE").distinct().sort(data["DISTANCE"].desc()).show()


# 4.) ORIGIN to DEST Max speed
# def calculate_avg_speed(df):
#     return df["DISTANCE"] / df["AIR_TIME"] * F.lit(60)
#
#
# data_with_avg_speed = data.select("ORIGIN", "DEST", "DISTANCE", "AIR_TIME")\
#     .withColumn("AVG_SPEED", calculate_avg_speed(data))\
#     .select("ORIGIN", "DEST","AVG_SPEED").groupBy("ORIGIN", "DEST").agg(F.avg("AVG_SPEED").alias("AVG_SPEED"))
#
# data_with_avg_speed.sort(data_with_avg_speed["AVG_SPEED"].desc()).explain()
#
#
# def calculate_avg_speed_py(distance, time):
#     return distance / time * 60.0
#
#
# calculate_avg_speed_udf = F.udf(calculate_avg_speed_py, T.DoubleType())
#
# data.select("ORIGIN", "DEST", "DISTANCE", "AIR_TIME") \
#     .withColumn("AVG_SPEED", calculate_avg_speed_udf(data["DISTANCE"], data["AIR_TIME"])) \
#     .select("ORIGIN", "DEST","AVG_SPEED").groupBy("ORIGIN", "DEST").agg(F.avg("AVG_SPEED").alias("AVG_SPEED")) \
#     .sort(F.col("AVG_SPEED").desc()).explain()


# 5.) Carrier with the most deleay / opt. reltetd to ORIGIN
# SFA AA avg(delay)

data.select("ORIGIN", "OP_UNIQUE_CARRIER", "DEP_DELAY")\
    .groupBy("ORIGIN","OP_UNIQUE_CARRIER")\
    .agg(F.avg(data["DEP_DELAY"]))\
    .sort(data["ORIGIN"].desc())\
    .show()

# 6.) Find the aitport witch are only dest



#
# carrier.printSchema()
#
# total_delay = data.filter(data["ARR_DELAY"] < 0)\
#     .groupBy("OP_UNIQUE_CARRIER")\
#     .agg(F.sum(data["ARR_DELAY"]).alias("TOTAL_DELAY"))\
#     .sort(F.col("TOTAL_DELAY"))
#
# total_delay.join(carrier, on=total_delay["OP_UNIQUE_CARRIER"] == carrier["Code"])\
#     .select(carrier["Description"], total_delay["TOTAL_DELAY"])
#
#
# data.createTempView("flights")
#
#spark.sql("SELECT OP_UNIQUE_CARRIER, sum(ARR_DELAY) AS TOTAL_DELAY FROM flights WHERE ARR_DELAY < 0 GROUP BY OP_UNIQUE_CARRIER")
# sum_1 = data.filter(data["ARR_DELAY"] < 0).agg(F.sum(data["ARR_DELAY"])).collect()[0]["sum(ARR_DELAY)"]
# print(sum_1)
#
#
# delay_data = data.filter(data["ARR_DELAY"] < 0).select(data["ARR_DELAY"]).collect()
# sum_2 = sum(map(lambda r: r["ARR_DELAY"], delay_data))
# print(sum_2)




