from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
from pyspark.sql.window import Window

spark = SparkSession.builder.appName("Adcam").getOrCreate()

data = spark.read.parquet("data/adcam_simu.parquet")
data.printSchema()
data.show(1000, False)


# start, end, warningppp, targetdecelerationppp,
def flatten_events(df):
    window = Window.partitionBy().orderBy("timestamp")

    df = df.select("timestamp", "warningppp", "targetdecelerationppp")
    df = df.withColumn("_delta", F.lag(df["warningppp"]).over(window))
    df = df.withColumn("_flip", (df["warningppp"] != df["_delta"]) | (df["_delta"].isNull()))
    df = df.withColumn("_start", F.when(df["_flip"], df["timestamp"]))
    df = df.withColumn("start", F.last(df["_start"], ignorenulls=True).over(window))

    return df.groupBy(df["start"]).agg(F.max(df["timestamp"]).alias("end"), F.avg(df["warningppp"]).alias("warningppp"), F.avg(df["targetdecelerationppp"]).alias("targetdecelerationppp"))


dut = flatten_events(data)

gt = data.select("GT_timestamp", "GT_wppp_category", "GT_wppp_category", "GT_wqva_category", "GT_wlta_category").filter(F.isnan(data["GT_timestamp"]) == False)
dut.join(gt, on=(gt["GT_timestamp"] >= dut["start"]) & (gt["GT_timestamp"] <= dut["end"]),how="full_outer").orderBy(dut["start"]).show()
