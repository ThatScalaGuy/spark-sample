from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql import types as T
#import matplotlib.pyplot as plt


spark = SparkSession.builder.appName("Lidar").getOrCreate()
spark.sparkContext
objects_with_signals = spark.read.parquet("data/sample_data_lidar/objects_with_signals.lidar")

objects_with_signals.printSchema()

#objects_with_signals = objects_with_signals.select("frame_id", "pair_id", "source", "x_coord")

dut = objects_with_signals.filter(objects_with_signals["source"] == "dut")

gt = objects_with_signals.filter(objects_with_signals["source"] == "ground_truth")\
    .groupBy("frame_id", "pair_id", "source")\
    .agg(F.stddev(objects_with_signals["x_coord"]).alias("x_coord")) \
    .sort(objects_with_signals["frame_id"])

########################################################################################################################

matched_data_df = gt.alias("gt").join(dut.alias("dut"),on=(F.col("gt.frame_id") == F.col("dut.frame_id")) & (F.col("gt.pair_id") == F.col("dut.pair_id")), how="full")

def calculate_std_avg_for_range(data, start, col_name, range_col_name):
    gt_range_col_name = "gt.{}".format(range_col_name)
    gt_col_name = "gt.{}".format(col_name)
    dut_col_name = "dut.{}".format(col_name)

    return lambda range: data.filter(F.col("gt.pair_id") == F.col("dut.pair_id")) \
        .filter((F.col(gt_range_col_name) >= F.lit(start)) & (F.col(gt_range_col_name) < F.lit(range)))\
        .select("gt.frame_id", "gt.pair_id", gt_col_name, dut_col_name).withColumn("diff", F.col(gt_col_name) - F.col(dut_col_name)) \
        .agg(F.avg(F.col("diff")).alias("bias"), F.stddev(F.col("diff")).alias("stddev")).withColumn("range", F.lit(range))


def acc_std_avg(data, start=4, ranges=[], col_name=None, range_col_name="x_coord"):
    ranged_dfs = list(map(calculate_std_avg_for_range(data,start, col_name, range_col_name), ranges))

    final_df = ranged_dfs[0]
    for df in ranged_dfs[1:]:
        final_df = final_df.union(df)

    return final_df


acc_std_avg(matched_data_df, start=15, ranges=[20, 40, 60], col_name="x_coord")

